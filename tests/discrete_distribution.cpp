// discrete_distribution
#include <Rcpp11>
#include <iostream>
#include <random>

using namespace Rcpp;

// [[export]]
int discrete_distribution()
{
  const int nrolls = 1000; // number of experiments
  const int nstars = 100;   // maximum number of stars to distribute

  std::default_random_engine generator;
  std::discrete_distribution<int> distribution {2,2,1,1,2,2,1,1,2,2};

  int p[10]={};

  for (int i=0; i<nrolls; ++i) {
    int number = distribution(generator);
    std::cout << number << ' ';
    
    ++p[number];
  }
  std::cout << std::endl;

  std::cout << "a discrete_distribution:" << std::endl;
  for (int i=0; i<10; ++i)
    std::cout << i << ": " << std::string(p[i]*nstars/nrolls,'*') << std::endl;

  return 0;
}


// [[export]]
NumericVector rngCppScalar() {
  NumericVector x(4);
  x[0] = R::runif(0,1);
  x[1] = R::rnorm(0,1);
  x[2] = R::rt(5);
  x[3] = R::rbeta(1,1);
  return(x);
}

// [[export]]
NumericMatrix rngCpp(const int N) {
  NumericMatrix X(N, 4);
  X(_, 0) = runif(N,2,5);
  X(_, 1) = rnorm(N);
  X(_, 2) = rt(N, 5);
  X(_, 3) = rbeta(N, 1, 1);
  return X;
}

// [[export]]
NumericVector runifCpp(const int N, const double min, const double max) {
  // NumericVector X(N);
  // X = 
  return runif(N,min,max);
}

// [[export]]
double runifCppScalar(const double min, const double max)
{
  return R::runif(min,max);
}

// [[export]]
int randInt(){
  return rand() % 100;
} 

// [[export]]
int sampleIntScalar(int N)
{
  return R::runif(0,1) * N;
}


// [[export]]
int sampleIntScalar2(int N)
{
  return static_cast <int> (R::runif(0,1) * N);
}
