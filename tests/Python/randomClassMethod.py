import numpy.random

class Number:
	def __init__(self,x):
		self.x = x
		self.methodNames = ('addOne',	'subOne')
		self.methodProbs = (   .7	,	   .3	)

	def __str__(self):
		return 'x\t= {0}'.format(self.x)

	def addOne(self):
		self.x += 1

	def subOne(self):
		self.x -= 1

	def randomMethod(self):
		selectedMethod = numpy.random.choice( self.methodNames, p = self.methodProbs )
		getattr( self, selectedMethod )()

N = Number(10)
print N
N.randomMethod()
print N
N.randomMethod()
print N