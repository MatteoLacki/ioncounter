precursor = 'PPQQPPQP'
print precursor

revPrecursor 	= precursor[::-1]
print revPrecursor

fragments 		= [revPrecursor[0]]
prolineChunk 	= revPrecursor[0] == 'P'

for i in xrange(1,len(revPrecursor)): #LGFFQQPKPR
	AA = revPrecursor[i]

	if  AA == 'P':		
		if prolineChunk:
			fragments[-1] += AA 			
		else:
			fragments.append('P')

		prolineChunk = True	

	else: 
		if prolineChunk:
			fragments[-1] += AA
		else:
			fragments.append(AA)		

		prolineChunk = False		

print fragments