class Test:
	def __init__(self, x, y):
		self.x = x
		self.y = y

	def __hash__(self):
		return hash((self.x,self.y))

	def __eq__(self, other):
		return (self.x, self.y) == (other.x, other.y) 

	def __str__(self):
		return 'x = {0}, y = {1}'.format( self.x, self.y )

T = Test(2,3)
S = Test(3,4)


dic = {}

print dic

dic[T] = 'sva'
dic[S] = 5


for el in dic:
	print el

print dic[T]
print dic[S]