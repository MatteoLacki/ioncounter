import numpy.random as npr

def trivialPartition(N):
	res = [0.0]
	res.extend( npr.uniform( size = N-1 ) )
	res.sort()
	res.append(1.0)

	return res


for i in xrange(100):
	trivialPartition(100000)