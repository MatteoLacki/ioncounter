import numpy.random as npr

def exponentPartition(N):
	res = [0.0]	
	S 	= 0.0
	
	for n in xrange(N-1):
		S += npr.exponential()
		res.append(S)
	
	res = [ x/S for x in res ]
	res.append(1.0)

	return res


for i in xrange(100):
	exponentPartition(100000)