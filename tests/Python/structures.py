class A:
	def __init__(self, x):
		self.x = x

L = []


class B(tuple):
	def __init__(self, elements):
		self.names 		= ('a','b')
		self.container 	= ( elements['a'], elements['b'])

	def a(self):
		return self.container[0]

	def b(self):
		return self.container[1]

	def __getitem__(self, name):
		if name in self.names:
			return getattr( self, name )()
		else:
			return 'Dupa.'	

	def __str__(self):
		return 'a = {0}, b = {1}'.format(self.a(), self.b())	



b = B({'a':12, 'b':15})
print b
print b['a']
print b['b']
print b['c']