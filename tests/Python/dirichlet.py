import numpy.random as npr
alpha = [1,5,1]
print npr.dirichlet(alpha)

w = ['str']
print w[-1]

input = {'ETD': 1.0, 'ETnoD': 1.0, 'HTR': 1.0, 'PTR': 1.0}
print input
print npr.dirichlet([input[x] for x in input])

print [x for x in input]
print npr.dirichlet(tuple([input[x] for x in input]))
