class Foo:
	pass

bar = Foo()
baz = Foo()

print id(bar)
print id(baz)

list_a = [1,2,3]
list_b = [1,2,3]

for v in list_a: 
	print id(v)

for v in list_b: 
	print id(v)

print id(list_a)
print id(list_b)