class Dupa:
	def __init__(self, value):
		self.value = value

	def plusOne(self):
		self.value += 1

	def __str__(self):
		return "Value\t= {0}".format( self.value )

D = Dupa(14)

print id(D)
D.plusOne()
print id(D)
print D

def ChangeDupa(D):
	D.plusOne()
	return D

ChangeDupa(D)
print D
print id(D)

D = ChangeDupa(D)
print D
print id(D)

def CopyDupa(D):
	AnotherD = ChangeDupa(D)
	return AnotherD

D = CopyDupa(D)
print D
print id(D)

def enlist(D):
	return [D]

L = enlist(D)
print id(L) != id(D)
print id(L[0]) == id(D)