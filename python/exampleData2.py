import example
import cPickle as pickle

precursor 		= 'RPKPQQFFGLM'
precursorNo 	= 100
charge 			= 3

				# ETD ETnoD HTR PTR
rProbs 		= [  .1,   .3,  .1, .5 ]
intensity 	= 1.0
Tmax 		= 1.0

DG = example.data( 
	precursor, precursorNo, charge, 
	rProbs, intensity, 
	Tmax
)

DG.make()

with open('exampleData2.txt','w') as f:
	pickle.dump( ( precursor, DG ), f)

# print DG.molCounts[2].most_common(10)[0][0]
