import numpy.random
# times = numpy.cumsum( 
# 	numpy.random.exponential( scale=1.0/7, size = 1000 )
# )

precursorCharge = 3
precursorNo 	= 1000
intensity 		= 1.0
U 				= float( precursorCharge * precursorNo ) * 2.0

Tmax = .1
T = 0
# times 	= [T]
charges = precursorCharge * precursorNo


for charges in reversed(xrange(1,precursorCharge*precursorNo+1)):
	T += numpy.random.exponential( scale= intensity/(charges) )
	times.append(T)

# while charges > 0 and T < Tmax:
# 	T += numpy.random.exponential( scale = intensity/(charges) )
# 	times.append(T)	
# 	charges -= 1

allJumps = [0.0]
prevTime = 0.0
time_idx = 1 # The second time.
chargesLeft = charges

N = 1000
times = [ x/float(N) for x in xrange(N)]


# print 'Liczba czasow reakcji = {0}'.format( len(times) )

while chargesLeft > 0 and time_idx < len(times):
	nextTime 	= times[ time_idx ]
	newJumpsNo 	= numpy.random.poisson( lam = (U - intensity * chargesLeft)*(nextTime-prevTime) )	
	
	if newJumpsNo > 0:
		newJumps = numpy.random.uniform(
			low 	= prevTime, 
			high 	= nextTime, 
			size 	= newJumpsNo  
		)
		newJumps.sort()		

		acceptedJumps 	= []			
		newJump_idx 	= 0	

		while chargesLeft > 0 and newJump_idx < len(newJumps):	
			p = intensity * chargesLeft / U
			
			if numpy.random.choice( a=(True,False), p=(p, 1.0-p) ):
				acceptedJumps.append( newJumps[newJump_idx] )	
				chargesLeft -= 1  	# Reaction at time newJumps[newJump_idx]

			newJump_idx += 1	
		
		allJumps.extend(acceptedJumps)	

			# Appending the original time.
		if chargesLeft > 0:
			p = intensity * chargesLeft / U
			if numpy.random.choice( (True,False), (p, 1.0-p) ):
				chargesLeft -= 1
				allJumps.append( nextTime )

	time_idx += 1
	prevTime = nextTime 

# print 'Laczna liczba skokow'
# print len(allJumps)
