import numpy.random

class Molecule:
	def __init__( self, charge, start, end, hydroDiff, residues ):
		self.charge = charge
		self.start 	= start
		self.end 	= end
		self.hydroDiff 	= hydroDiff
		self.residues 	= residues
		self.reactionNames 	= ('ETD', 'ETnoD', 'HTR', 'PTR')


	def __str__(self):
		return "charge\t\t= {0}\nstart\t\t= {1}\nend\t\t\t= {2}\nhydroDiff\t= {3}\n".format( self.charge, self.start, self.end, self.hydroDiff ) 

	def reduceCharge(self):
		self.charge -= 1

	def addH(self):
		self.hydroDiff += 1

	def substractH(self):
		self.hydroDiff -= 1

	def __hash__(self):
		return hash( ( self.charge, self.start, self.end, self.hydroDiff ) )
		
	def __eq__(self, other):
		return ( self.charge, self.start, self.end, self.hydroDiff ) == \
		( other.charge, other.start, other.end, other.hydroDiff )

	def ETD(self):
		self.reduceCharge()	

		if self.charge > 1:
			firstBond 	= self.start + 1 # we severe an existing bond.
			
				# Choose bond to brake.
			bBond = numpy.random.randint( low = firstBond,  high = self.end ) 	
				# self.startend = lastBond + 1, randint will never draw it.

			cResidualsNo = sum( self.residues[ firstBond:bBond ] ) 
			zResidualsNo = sum( self.residues[ (bBond+1):self.end ] )
				
			totalResidues= float(cResidualsNo + zResidualsNo)
						
				# Redistributing of charges.
			cCharge = numpy.random.binomial( 
				n = self.charge, 
				p = cResidualsNo/totalResidues #=0 if it's a C0 fragment.
			)	
			zCharge = self.charge - cCharge
				
				# Redistribution of hydroDiffs.
			if self.hydroDiff > 0:
				cHydroDiff = numpy.random.binomial( 
					n = self.hydroDiff, 
					p = cResidualsNo/totalResidues
				)
				zHydroDiff = self.hydroDiff - cHydroDiff
			else:
				cHydroDiff = 0
				zHydroDiff = 0

			return [ 
				Molecule( cCharge, self.start,	bBond, 	cHydroDiff, self.residues ), 
					# Probably this could be optimised by using already existing space. 
				Molecule( zCharge, bBond, 	self.end, 	zHydroDiff, self.residues ) 
			]
		else:
			return [self] 	# Here we could do better, but it is not observable at all for now. 
						# This could still be interesting for the analysis of losses.		
	
	def ETnoD(self):
		self.reduceCharge()
		self.addH()
		return [self]

	def HTR(self):
		fragments = self.ETD()
		
		if len(fragments) == 1: # only one molecule: it's charge==1 
			return fragments
		else:
			fragments[0].substractH()	# C ion
			fragments[1].addH()			# Z ion
			return fragments

	def PTR(self):
		self.reduceCharge()
		return [self]	

	def react( self, reactionProbs ):		
		reactionTag = numpy.random.choice( 
			a = self.reactionNames, 
			p = reactionProbs 
		)		
		return ( reactionTag, getattr( self, reactionTag)() )

	# def reset(self, charge, start, end, hydroDiff ):
	# 	self.charge = charge
	# 	self.start 	= start
	# 	self.end 	= end
	# 	self.hydroDiff = hydroDiff 

# import parse
# precursor = 'RPKPQQFFGLM'
# superAAs, residues = parse.precursor(precursor)

# M = Molecule( charge = 3, start = -1, end = 10, hydroDiff = 0, residues = residues )

# print M
# res = M.react(reactionProbs = [.1, .5, .1, .3])
# print res[0]

# for m in res[1]:
# 	print m