import cPickle as pickle
# precursor, E = pickle.load( open('exampleData.txt', 'rb') )
precursor, E = pickle.load( open('exampleData2.txt', 'rb') )

print E.times[-1]
print len(E.times)
print E.intensity
print E.rProbs

res = [E.reactionsCnt[r] for r in ('ETD','ETnoD','HTR','PTR') ]
print [float(r)/sum(res) for r in res]

print E.intensity
oldAlpha 	= 1.0 
oldBeta 	= 1.0

charges 	= E.charge * E.precursorNo 			
prevTime 	= E.times[0]
timeCharge 	= 0.0
for time_idx in xrange( 1, len(E.times) ):		
	nextTime 	=  E.times[time_idx]
	timeCharge 	+= (nextTime-prevTime)*charges
	charges 	-= 1
	prevTime 	=  nextTime

alpha 	= oldAlpha + sum(res)
beta 	= oldBeta + timeCharge

print alpha/beta
print alpha/beta**2