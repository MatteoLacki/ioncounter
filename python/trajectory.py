import numpy.random as npr
import math 

import molecule
from collections import Counter

class NegativeValue(Exception):
	pass

class Trajectory:
	'This class provides the trajectory creator.'
	
	def __init__(self, 
		data, 
		charge, precursorNo, residues, 
		times, 
		intensity, rProbs, 		
		Uconst, epsilon
	):				
		bondsNo = len(residues) - 1

		self.data 		= data
		self.epsilon 	= epsilon
		self.charge		= charge
		self.precursorNo= precursorNo
		self.intensity 	= intensity
		self.rProbs		= rProbs
		self.Uconst 	= Uconst

		self.L = [ [] for x in xrange(charge+1) ]
			# Fills in the precursors.
		for i in xrange(precursorNo): 	
		    self.L[ charge ].append(
		    	molecule.Molecule( 
		    		charge, 	
		    		-1, 		# N terminal 
		    		bondsNo, 	# C terminal
		    		0, 			# no electron holes filled.
		    		residues
		    	) 
		    )

			# 'Initialising times of reactions.'
		self.times = self.getTimes( times )
	
	def getTimes(self, times):			
		allJumps = [0.0]
		prevTime = 0.0
		time_idx = 1

		chargesLeft = self.charge * self.precursorNo
		U 			= float(chargesLeft) * self.Uconst * self.intensity 

		while chargesLeft > 0 and time_idx < len(times):
			nextTime 	= times[ time_idx ]

			if U - self.intensity * chargesLeft < 0:
				print 'U = ', U
				print 'I = ', self.intensity
				print 'ChargeLeft = ', chargesLeft
				raise NegativeValue()

			newJumpsNo 	= npr.poisson( lam = (U - self.intensity * chargesLeft)*(nextTime-prevTime) )		
			if newJumpsNo > 0:
				newJumps = npr.uniform(
					low  = prevTime, 
					high = nextTime, 
					size = newJumpsNo  
				)
				newJumps.sort()		

				acceptedJumps 	= []			
				newJump_idx 	= 0	

				while chargesLeft > 0 and newJump_idx < len(newJumps):	
					p = self.intensity * chargesLeft / U
					
					if npr.choice( a=(True,False), p=(p, 1.0-p) ):
						acceptedJumps.append( newJumps[newJump_idx] )	
						chargesLeft -= 1  	# Reaction at time newJumps[newJump_idx]						
				
					newJump_idx += 1	
				
				allJumps.extend(acceptedJumps)	

					# append the original nextTime? 
				if time_idx < len(times)-1: 
					if chargesLeft > 0: 	
						p = self.intensity * chargesLeft / U
						if npr.choice( (True,False), (p, 1.0-p) ):
							chargesLeft -= 1
							allJumps.append( nextTime )
			time_idx += 1
			prevTime = nextTime 
		allJumps.append( times[-1] ) # always include Tmax. 

		return allJumps 


	def getRandomMolecule(self):
			# Probabilites of choosing molecule with charge state from 1 to charge.
		p = [ x * len(self.L[x]) for x in xrange( len(self.L) ) ]
			# Statistical sum.
		S = float( sum(p) ) 

		# print p
		# print S
		if S > 0:
			p = [x/S for x in p]
			chargeList = npr.choice( a = self.L, p = p )	# Choice of charge state list.
			
			i = npr.randint( 0, len(chargeList) ) 	# Choice of element from that list.
			ret = chargeList[i]

			if i == (len( chargeList ) - 1):
				chargeList.pop()  
			else:
				chargeList[i] = chargeList.pop()
			return ret
		else:
			raise StopIteration()



	def make(self):
		self.reactionsCnt 	= Counter()

		try: # 1:(len( self.reactionTimes )-2) # Exclude T0 = [0.0] and Tmax
			for cReactionNo in xrange(1, len( self.times )-1 ): 
				M 			= self.getRandomMolecule()
				products 	= M.react( self.rProbs )		
					# products[0] = reaction type idx
					# products[1] = list of products itself
							
				for mol in products[1]:
					self.L[mol.charge].append(mol)

				self.reactionsCnt[ products[0] ] += 1

				# if currentReactionNo % 1000 == 0:
					# print currentReactionNo

		except StopIteration:
			# print 'No more charges. Strange and spooky.'
			pass

		self.molCounts = [ Counter() for x in xrange(self.charge+1) ]
		
		for idx in xrange( self.charge+1 ):
			for mol in self.L[idx]:
				self.molCounts[idx][mol] += 1

	def getLogLikelihood(self):
		logLikelihood = 0.0

		# print 'Calculating logLikelihood.'
		for idx in xrange(1, self.charge+1): # Only charged molecules.
			for mol in self.data[idx]:
				X = self.molCounts[idx][mol] 	# Simulated X
				A = self.data[idx][mol]			# real data
				logLikelihood += A * math.log(X + self.epsilon)

		self.logLikelihood = logLikelihood


def make(
	data, 
	charge, precursorNo, residues, 
	times, 
	intensity, rProbs, 		
	Uconst, epsilon
):
	T = Trajectory( data, charge, precursorNo, residues, times, 
		intensity, rProbs, Uconst, epsilon)
	T.make()
	T.getLogLikelihood()

	return T
# import cPickle as pickle
# example = pickle.load( open('exampleData.txt', 'rb') )

# T = Trajectory( 
# 	data 		= example.molCounts,
# 	charge 		= example.charge, 
# 	precursorNo = example.precursorNo, 
# 	residues 	= example.residues, 
# 	times 		= example.times, 
# 	intensity 	= example.intensity, 
# 	rProbs 	= example.rProbs,
# 	Uconst 		= 1.5,
# 	epsilon 	= .01
# )
# T.make()

# with open('exampleTrajectory.txt','w') as f:
# 	pickle.dump( T, f)

# T = pickle.load( open('exampleTrajectory.txt', 'rb') )
# print T.getLikelihood()