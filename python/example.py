import 	parse
import 	molecule
import 	numpy.random as npr
from 	collections import Counter


class data:
	'This class generates data in silico.'
	
	def __init__( 
		self, 
		precursor, precursorNo, charge, 
		rProbs, intensity, 
		Tmax = 1.0
	):		
		self.charge 	= charge		
		self.precursorNo= precursorNo
		self.intensity	= intensity
		self.rProbs 	= rProbs
		self.Tmax = Tmax

		superAAs, self.residues = parse.precursor(precursor)
		bondsNo = len(self.residues)-1

		self.L = [ [] for x in xrange(charge+1) ]
		for i in xrange(precursorNo): 	# Fills in the precursors.
		    self.L[charge].append(
		    	molecule.Molecule( 
		    		charge, 	
		    		-1, 		# N terminal 
		    		bondsNo, 	# C terminal
		    		0, 			# no electron holes filled.
		    		self.residues
		    	) 
		    )

	def getRandomMolecule(self):
			# Probabilites of choosing molecule with charge state from 1 to charge.
		p = [ x * len(self.L[x]) for x in xrange( len(self.L) ) ]
			# Statistical sum.
		S = float( sum(p) ) 

		if S > 0:
			p = [x/S for x in p]
			chargeList 	= npr.choice( self.L, p = p )	# Choice of charge state list.
			
			i = npr.randint( 0, len(chargeList) ) 	# Choice of element from that list.
			ret = chargeList[i]

			if i == (len( chargeList ) - 1):
				chargeList.pop()  
			else:
				chargeList[i] = chargeList.pop()
			return ret
		else:
			raise StopIteration()

	def make(self):
		cReactionNo = 0		
		charges = self.precursorNo * self.charge
		t 			= 0.0	
		self.times 	= [t]
		self.reactionsCnt = Counter()

		while charges > 0 and t < self.Tmax:
			cReactionNo += 1
			if cReactionNo % 100 == 0:
				print cReactionNo
			
				# Generating reaction moment:
			t += npr.exponential( scale= 1.0/(self.intensity*charges) )  

			if t < self.Tmax:
				self.times.append(t)

					# Generating molecule:
				M 		= self.getRandomMolecule() 
				products= M.react( self.rProbs )			
					# products[0] = reaction type idx
					# products[1] = list of products itself
					
				charges -= 1

					# Updating trajectory:
				for mol in products[1]:
					self.L[mol.charge].append(mol)

				self.reactionsCnt[ products[0] ] += 1
		
		self.molCounts = [ Counter() for x in xrange(self.charge+1) ]
		
		for idx in xrange( self.charge+1 ):
			for mol in self.L[idx]:
				self.molCounts[idx][mol] += 1
