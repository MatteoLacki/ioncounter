import parser
import numpy.random
import cPickle as pickle

from collections import Counter

precursor = 'RPKPQQFFGLM'
superAAs, residues = parser.parse(precursor)

# print precursor
# print superAAs 
# print residues

bondsNo 		= len(residues)-1 	# The number of the non-existing extra to the right bond.

charge = 3
precursorNo 	= 1000
intensity 		= 1

reactionsNo 	= 1000 # This number will be dictated by the times method. 

reactionProbs 	= [.1, .5, .1, .3] 	# [ ETD, ETnoD, HTR, PTR ]

currentReactionNo = 0

class Molecule:
	def __init__( self, charge, start, end, hydroDiff ):
		self.charge = charge
		self.start 	= start
		self.end 	= end
		self.hydroDiff = hydroDiff

	def __str__(self):
		return "charge\t\t= {0}\nstart\t\t= {1}\nend\t\t\t= {2}\nhydroDiff\t= {3}\n".format( self.charge, self.start, self.end, self.hydroDiff ) 

	def reduceCharge(self):
		self.charge -= 1

	def addH(self):
		self.hydroDiff += 1

	def substractH(self):
		self.hydroDiff -= 1

	def __hash__(self):
		return hash( ( self.charge, self.start, self.end, self.hydroDiff ) )
		
	def __eq__(self, other):
		return ( self.charge, self.start, self.end, self.hydroDiff ) == \
		( other.charge, other.start, other.end, other.hydroDiff )

	# def reset(self, charge, start, end, hydroDiff ):
	# 	self.charge = charge
	# 	self.start 	= start
	# 	self.end 	= end
	# 	self.hydroDiff = hydroDiff 


########################################################################################
###### Molecules Container #############################################################
########################################################################################

L = [[] for x in xrange(charge+1)]
for i in xrange(precursorNo): 	# Fill in the precursors.
    L[charge].append( 	# N and C termini 	# no electron holes filled.
    	Molecule( charge, -1, bondsNo, 	0 ) 
    ) 
# print L[3][0]

def getRandomMolecule():
	p = [ x * len( L[x] ) for x in xrange(len(L)) ]
	S = float(sum(p))

	if S > 0:
		p = [x/S for x in p]
		chargeList 	= numpy.random.choice( L, p = p )	# Choice of charge state list.
		# print p

		i = numpy.random.randint( 0, len(chargeList) ) 	# Choice of element from that list.
		ret = chargeList[i]

		if i == (len( chargeList ) - 1):
			chargeList.pop()  
		else:
			chargeList[i] = chargeList.pop()
		return ret
	else:
		raise StopIteration()
# print getRandomMolecule()


########################################################################################
###### REACTIONS #######################################################################
########################################################################################

ReactionNames 	= ( 'ETD', 'ETnoD', 'HTR', 'PTR' )
ReactionsNo 	= len(ReactionNames)
ReactionsCnt 	= [ 0 for x in xrange(ReactionsNo) ] 


def ETD(m):
	m.reduceCharge()

	if m.charge > 1:
		firstBond 	= m.start + 1 	# we severe an existing bond.
		
			# Choose bond to brake.
		bBond = numpy.random.randint( low = firstBond,  high = m.end ) 	
			# m.end = lastBond + 1, randint will never draw it.

		cResidualsNo = sum(residues[ firstBond:bBond ]) 
		zResidualsNo = sum(residues[ (bBond+1):m.end ])
			
		totalResidues= float(cResidualsNo + zResidualsNo)
					
			# Redistributing of charges.
		cCharge = numpy.random.binomial( 
			n = m.charge, 
			p = cResidualsNo/totalResidues #=0 if it's a C0 fragment.
		)	
		zCharge = m.charge - cCharge
			
			# Redistribution of hydroDiffs.
		if m.hydroDiff > 0:
			cHydroDiff = numpy.random.binomial( 
				n = m.hydroDiff, 
				p = cResidualsNo/totalResidues
			)
			zHydroDiff = m.hydroDiff - cHydroDiff
		else:
			cHydroDiff = 0
			zHydroDiff = 0

		return [ 
			Molecule( cCharge, m.start,	bBond, 	cHydroDiff ), # Probably this could be optimised by using already existing space. 
			Molecule( zCharge, bBond, 	m.end, 	zHydroDiff ) 
		]
	else:
		return [m] # Here we could do better, but it is not observable at all for now. 
		# This could still be interesting for the analysis of losses.

def ETnoD(m):
	m.reduceCharge()
	m.addH()
	return [m]

def HTR(m):
	fragments = ETD(m)
	
	if len(fragments) == 1: # only one molecule: it's charge==1 
		return fragments
	else:
		fragments[0].substractH()	# C ion
		fragments[1].addH()			# Z ion
		return fragments

def PTR(m):
	m.reduceCharge()
	return [m]

Reactions 		= ( ETD, ETnoD, HTR, PTR ) # Tupple, for this will not change.


def react(M):
	reactionId = numpy.random.choice( a = ReactionsNo, p = reactionProbs )
	return (reactionId, Reactions[reactionId](M))
# res = react(L[3][0])
# print ReactionNames[res[0]]
# for obj in res[1]:
# 	print obj


########################################################################################	
##### Trajectory #######################################################################
########################################################################################

def makeTrajectory( reactionsNo ):	
	currentReactionNo = 0	

	try:
		while currentReactionNo < reactionsNo:
			currentReactionNo += 1

			M = getRandomMolecule()
			products = react(M)		
				# products[0] = reaction type idx
				# products[1] = list of products itself
						
			for mol in products[1]:
				L[mol.charge].append(mol)

			ReactionsCnt[products[0]] += 1

			if currentReactionNo % 100 == 0:
				print currentReactionNo
				print ReactionsCnt
				print [ len(l) for l in L]
	except StopIteration:
		pass

	molCounts = Counter()

	for chargeList in L:
		for mol in chargeList:
			molCounts[mol] += 1

	return molCounts

# makeTrajectory(100)
# for x in molCounts.most_common():
# 	print x[0], x[1]

# with open('result.txt','w') as f:
# 	pickle.dump( ReactionsCnt, f)
# Results = ({} for x in xrange(charge+1))

########################################################################################	
##### Simulation #######################################################################
########################################################################################


