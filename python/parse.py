def precursor( precursor ):
	revPrecursor 	= precursor[::-1]
	fragments 		= [revPrecursor[0]]
	prolineChunk 	= revPrecursor[0] == 'P'

	for i in xrange( 1, len(revPrecursor)): #LGFFQQPKPR
		AA = revPrecursor[i]
		
		if  AA == 'P':		
			if prolineChunk:
				fragments[-1] += AA 			
			else:
				fragments.append('P')

			prolineChunk = True	

		else: 
			if prolineChunk:
				fragments[-1] += AA
			else:
				fragments.append(AA)		

			prolineChunk = False		

	if precursor[0] != 'P':
		fragments.append('')
	fragments = [ x[::-1] for x in fragments[::-1] ]
	residues  = [ len(x) for x in fragments ]

	return ( fragments, residues )	


def reactions(x):
	keys 	= ('ETD','ETnoD','HTR','PTR')
	values 	= tuple( [ x[k] for k in keys ] )
	return values	


def intensity(x):
	keys 	= ('alpha', 'beta')
	values 	= tuple( [ x[k] for k in keys ] )
	return values

# print reaction({'ETnoD': 1.0, 'ETD': 2.0, 'HTR': 1.0,'PTR': 4.1})


# class reaction(tuple):
# 	def __init__(self, x):
# 		self.keys 	= ('ETD','ETnoD','HTR','PTR')
# 		self.values = tuple( [ x[k] for k in self.keys ] )
	
# 	def ETD(self):
# 		return self.values[0]		

# 	def ETnoD(self):
# 		return self.values[1]

# 	def HTR(self):
# 		return self.values[2]

# 	def PTR(self):
# 		return self.values[3]

# 	def __getitem__(self, key):
# 		if key in self.keys:
# 			return getattr( self, key )()
# 		else:
# 			return 'No such reaction. Have a good day.'

# 	def __str__(self):
# 		ret = ''
# 		for k in self.keys[0:-1]:
# 			ret += k + ' = ' + str(self[k]) + ', '
# 		lastKey = self.keys[-1]
# 		return ret + lastKey + ' = ' + str(self[lastKey]) + '\n'
