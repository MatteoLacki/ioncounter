import simulation
import cPickle as pickle
precursor, example = pickle.load( open('exampleData.txt', 'rb') )

reactionsParams = {'ETD': 1, 'ETnoD': 1, 'HTR': 1, 'PTR': 1} # user should provide all parameters: smaller list will result in error.	 
intensityParams = {'alpha': 1.0, 'beta': 1.0}

# print example.reactionProbs 

S = simulation.Simulation(
	data 		= example.molCounts,	
	precursor 	= 'RPKPQQFFGLM', 
	precursorNo = example.precursorNo, 	# We set the number of them bloody molecules.
	charge 		= example.charge,
	rParam 		= reactionsParams, 			# We don't set up the reactions.
	iParam  	= intensityParams, 	# We set up the bloody intensity as it was? We shouldn't for sure.
	Uconst 		= 1.5,
	trajNo 		= 10,
	timeGridNo 	= 1000,
	Tmax 		= 1.0,
	epsilon 	= .01,
	stepsNo 	= 1000
)

S.run()

with open('examplarySimulation.txt','w') as f:
	pickle.dump( S, f)
