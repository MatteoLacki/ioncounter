import molecule
import numpy.random as npr
import parse
import trajectory
import math 

#  Assumed order of inputs: ('ETD','ETnoD','HTR','PTR')

class Simulation:
	def __init__(self, 
		data,
		precursor, precursorNo, charge, 
		rParam 		= {'ETD': 1.0, 'ETnoD': 1.0, 'HTR': 1.0, 'PTR': 1.0}, # user should provide all parameters: smaller list will result in error.	
		iParam 	 	= {'alpha': 100.0, 'beta': 100.0},
		Uconst 		= 1.5, 
		trajNo 		= 10,
		timeGridNo 	= 1000, Tmax = 1.0,
		epsilon 	= .01,
		stepsNo		= 10000
	):
		self.superAAs, self.residues = parse.precursor( precursor )

#TO DO: THIS SHOULD BE TAKEN FROM MOLECULE.
		self.rParamsNames = ('ETD','ETnoD','HTR','PTR')
#TO DO: Raise an exception if some arg is missing!
			
			# iParam[0]=alpha, iParam[1]=beta
		self.iHyperParam = parse.intensity( iParam )
		# self.iParams = [ parse.intensity( iParam ) ]
			# rParam[0]=ETD, rParam[1]=ETnoD, rParam[2]=HTR, rParam[3]=PTR
		self.rHyperParam = parse.reactions( rParam )
		# self.rParams = [ parse.reactions( rParam ) ]
	
		self.intensity = []
		self.reactions = []
		
		self.dIntensity( self.iHyperParam )
		self.dReactions( self.rHyperParam )

		self.epsilon = epsilon
		self.stepsNo = stepsNo
		self.trajNo  = trajNo
		self.Tmax 	 = Tmax
		self.timeGridNo = timeGridNo

		self.lastT = trajectory.make(
			data 		= data, 
			charge 		= charge, 
			precursorNo = precursorNo, 
			residues 	= self.residues,
			times 		= self.initTime(),
			intensity 	= self.intensity[-1], 
			rProbs 		= self.reactions[-1],
			Uconst 		= Uconst, 
			epsilon 	= epsilon
		)

	def initTime(self): 							
		return [ # Tmax included at list's end.
			self.Tmax * x / float(self.timeGridNo) 
			for x in xrange(self.timeGridNo+1) 
		] 

		# Draws from \propto x^{\alpha-1} e^{-\beta x}
	def dIntensity(self,iParam):
		alpha, beta = iParam
		self.intensity.append(
			npr.gamma(
				shape = alpha, 
				scale = 1.0 / beta
			)
		)

	def dReactions(self,rParam):
		res = npr.dirichlet( rParam )
		self.reactions.append( 
			tuple(res)
		)


	def normalise(self, numbers):
		res = [ n - max(numbers) for n in numbers]
		nor = res[:]
		nor.sort()
		nor = [ math.exp(n) for n in nor ]
		S 	= math.log(sum(nor))

		return [ math.exp(r-S) for r in res ]


	def updateHyperParams(self):
		hyperAlpha, hyperBeta 	= self.iHyperParam
		oldrParams 				= self.rHyperParam

		T = self.lastT # Last accepted trajectory.
	
			# timeCharge = sum_{i=1}^{m+1} (t_i - t_{i-1} TotalCharge(t_{j-1}))
		charges 	= T.charge * T.precursorNo 			
		prevTime 	= T.times[0]
		timeCharge 	= 0.0
		for time_idx in xrange( 1, len(T.times) ):		
			nextTime 	=  T.times[time_idx]
			timeCharge 	+= (nextTime-prevTime)*charges
			charges 	-= 1
			prevTime 	=  nextTime

		reactionsNo = 0
		newrParams 	= []

		for n in xrange( len( self.rParamsNames ) ):	
			reactionNo 	=  T.reactionsCnt[ self.rParamsNames[n] ]
			reactionsNo += reactionNo
			newrParams.append( oldrParams[n] + reactionNo )   
		
		alpha = hyperAlpha + reactionsNo
		beta  = hyperBeta  + timeCharge
		
		self.dIntensity( (hyperAlpha, hyperBeta) )
		self.dReactions( tuple( newrParams ) )

	def run(self):
		for step in xrange(self.stepsNo):
			
			print step

			self.updateHyperParams()

			proposals = [
				trajectory.make(
					data 		= self.lastT.data, 
					charge 		= self.lastT.charge,  
					precursorNo = self.lastT.precursorNo,
					residues 	= self.residues,
					times 		= self.lastT.times,
					intensity 	= self.intensity[-1],
					rProbs		= self.reactions[-1],
					Uconst 		= self.lastT.Uconst,
					epsilon 	= self.lastT.epsilon
				) for n in xrange(self.trajNo)
			]

			logLikelihoods = [ T.logLikelihood for T in proposals ]
			logLikelihoods.append( self.lastT.logLikelihood )
			logLikelihoods = self.normalise( logLikelihoods )
	
			proposals.append( self.lastT )
			
			self.lastT = npr.choice( a = proposals, size = 1, p = logLikelihoods)[0]
