#include <vector>
#include <iostream>
#include <Rcpp11>
// #include <Rcpp.h>
// #include <unordered_map>

using namespace Rcpp;
using namespace std;

// typedef 	std::unordered_map<std::string,int> HashTable;

struct configuration
{
	int electronHoles;
	int start;
	int end; 

	configuration(
		const int electronHoles, 
		const int start, 
		const int end
	): electronHoles(electronHoles), start(start), end(end)
	{}
};

struct CHARGEdistr
{
	int* distr;
	
	CHARGEdistr(
		const int initialCharge,
		const int precursorNo
	): 	distr(new int[initialCharge])
	{
		for(int i=0; i<initialCharge; i++) distr[i] = 0;				
		distr[initialCharge-1] = precursorNo;
	}

	~CHARGEdistr()
	{
		delete[] distr;		
	}
};


typedef std::vector<configuration> CONFIG;

class 	simulator
{
private:
	CHARGEdistr	chargeD;	// Tools for getting pointer to appropriate list.
	CONFIG** 	data;		// Array with vectors with elements.

	int getRandomIndex(int N)
	{
	  return R::runif(0,1) * N;
	}


public:

	simulator( 
		const int initialCharge,
		const int precursorNo
	): 
	data(new CONFIG[initialCharge]), chargeD(initialCharge, precursorNo)
	{
		for( int i=0; i<initialCharge; i++ )
		{
			data[i] = new CONFIG;   							
		}

	}
};


// int main( const int initialCharge, const int precursorNo)
int main()
{
	return 0;	
};


